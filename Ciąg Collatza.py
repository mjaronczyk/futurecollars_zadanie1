
x = 209032090
while (x > 100 or x < 0):
    if x == 209032090:
        print("Podaj liczbe od 1 do 100: ")
    else:
        print("Liczba spoza przedzialu 1-100!")
    try:
        x = int(input())
    except:
        print("Spróbuj jeszcze raz, tym razem uzyj tylko cyfr!")
        continue

while x != 1:
    if x % 2 == 0:
        print(f'{x} /2 = {x/2}')
        x /= 2
    else:
        print(f'{x} * 3 + 1 = {3 * x + 1}')
        x = 3 * x + 1

