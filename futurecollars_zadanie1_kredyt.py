# Twoja pozostała kwota kredytu to X, to Y mniej niż w poprzednim miesiącu.


kwota = 12000
kwota2 = 0
rata = 200 
oprocentowanie = 3/100/12 # /12 bo miesieczne


inflacja = [
1.592824484, 
-0.453509101,
2.324671717,
1.261254407,
1.782526286,
2.329384541,
1.502229842,
1.782526286,
2.328848994,
0.616921348,
2.352295886,
0.337779545,
1.577035247,
-0.292781443,
2.48619659,
0.267110318,
1.417952672,
1.054243267,
1.480520104,
1.577035247,
-0.07742069,
1.165733399,
-0.404186718,
1.499708521,
]

lata = [1, 2]
miesiace = ['styczen', 'luty', 'marzec', 'kwiecien', 'maj', 'czerwiec', 
            'lipiec', 'sierpien', 'wrzesien', 'pazdziernik', 'listopad', 'grudzien',]

index = 0
for rok in lata:
    for miesiac in miesiace:
        kwota = kwota + (kwota*oprocentowanie) + (inflacja[index]*kwota/100/12) - rata
        splata = kwota - (kwota + (kwota*oprocentowanie) + (inflacja[index]*kwota/100/12) - rata)
        print(f'rok: {rok}, {miesiace[index%12]}', sep=', ')
        print(f'Twoja pozostała kwota kredytu to {kwota}, to {splata} mniej niż w poprzednim miesiącu.\n')
        index += 1

        